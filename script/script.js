// Чому для роботи з input не рекомендується використовувати клавіатуру?
// це може бути ненадійно через те, що не завжди використовується клавіатура як спосіб введення інформації, а також можуть бути казуси через різну розкладку.

// const buttonWrapper = document.querySelector('.btn-wrapper');
// const buttonArray = [...buttonWrapper.children].map(element => {
//     return element.textContent.toLowerCase()
// });

// let previosKey;
// const previosKeyColor = '#000000'
// const activeKeyColor = 'blue'
// document.addEventListener('keyup', function (event) {
//     activeKey = event.key.toLowerCase()
//     if (keyCheck(activeKey, buttonArray)) {
//         if (activeKey !== previosKey) {
//             if (previosKey) {
//                 changeColor(buttonArray.indexOf(previosKey), previosKeyColor);
//             }
//             changeColor(buttonArray.indexOf(activeKey), activeKeyColor);
//             previosKey = activeKey;
//         }
//     } else {
//         if (previosKey) {
//             changeColor(buttonArray.indexOf(previosKey), previosKeyColor);
//             previosKey = null;
//         }
//     }
// });
// function changeColor(changeEl, colorEl) {
//     if (changeEl >= 0) {
//         console.log(changeEl);
//         buttonWrapper.children[changeEl].style.backgroundColor = colorEl
//     }
// }
// function keyCheck(activeKey, defaultKeys) {
//     if (defaultKeys.includes(activeKey)) {
//         return true
//     }
//     return false
// }


const buttons = document.querySelectorAll('.btn');
let previousButton;

document.addEventListener('keydown', (e) => {
    const key = e.key.toLowerCase();
    const correspondingButton = Array.from(buttons).find(button => button.dataset.key === key);

    if (correspondingButton) {
        resetButtonHighlights();
        correspondingButton.classList.add('highlight');
        previousButton = correspondingButton;
    } else {
        resetButtonHighlights();
        previousButton = null;
    }
});

function resetButtonHighlights() {
    buttons.forEach(button => {
        button.classList.remove('highlight');
    });
}


